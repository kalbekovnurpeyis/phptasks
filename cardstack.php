<?php

$card = $_POST['number'];

$data['status'] = false;

$cards = ['валет', 'дама', 'король', 'туз'];

if (is_numeric($card)) {
    if ($card >= 6 && $card <=14) {
        $data['status'] = true;

        if ($card < 11) {
            $data['result'] = $card;
        } else {
            $data['result'] = $cards[$card - 11];
        }
    } else {
        $data['result'] = 'Введите число между 6 и 9!';
    }
} else {
    $data['result'] = 'Введите число!';
}

echo json_encode($data);