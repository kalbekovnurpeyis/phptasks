<?php

$degrees = $_POST['degrees'];

$data['status'] = false;

if (is_numeric($degrees)) {
    if ($degrees >= 0 && $degrees <= 360) {
        $hours = intval($degrees / 30);
        $minutes = intval(($degrees - $hours * 30) / .5);

        function clockVal($d) {
            if ($d < 10) {
                return 0 . $d;
            }

            return $d;
        }

        $data['status'] = true;
        $data['result'] = clockVal($hours) . ' : ' . clockVal($minutes);
    } else {
        $data['result'] = 'Число может быть от 0 до 360!';
    }
} else {
    $data['result'] = 'Введите число!';
}

echo json_encode($data);