<?php

$digit = $_POST['digit'];
$number = $_POST['number'];

$data['status'] = false;

if (is_numeric($number) && is_numeric($digit)) {

    $result = substr_count($number, $digit);

    if ($result > 0) {
        $data['status'] = true;
        $data['result'] = $result;
    } else {
        $data['result'] = 'Число не содержит данной цифры!';
    }
} else {
    $data['result'] = 'Введите цифры!';
}

echo json_encode($data);