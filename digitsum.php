<?php

$datas = $_POST['number'];

$data['status'] = false;

if (is_numeric($datas)) {
    $digitSum = 0;

    for ($i = 0; $i < strlen($datas); $i++) {
        if (is_numeric($datas[$i])) {
            $digitSum += $datas[$i];
        }
    }

    $data['status'] = true;
    $data['result'] = $digitSum;
} else {
    $data['result'] = 'Введите число!';
}

echo json_encode($data);
