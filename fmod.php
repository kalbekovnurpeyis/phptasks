<?php

$minNum = $_POST['minNum'];
$maxNum = $_POST['maxNum'];
$number = $_POST['number'];

$data['status'] = false;

if (is_numeric($minNum) && is_numeric($maxNum) && is_numeric($number)) {
    if ($minNum < $maxNum) {
        $start = $minNum;
        $end = $maxNum;
    } else {
        $start = $maxNum;
        $end = $minNum;
    }

    if ($number <= $end) {
        $resultSum = 0;

        for ($i = $start; $i <= $end; $i++) {
            if (fmod($i, $number) == 0) {
                $resultSum += $i / $number;
            }
        }

        $data['status'] = true;
        $data['result'] = $resultSum;
    } else {
        $data['result'] = 'Число делитель больше этих значений!';
    }
} else {
    $data['result'] = 'Введите числовое значение!';
}

echo json_encode($data);