<?php

$data['status'] = false;

if (!empty($_POST['fullname'])) {
    $fullname = explode(" ", $_POST['fullname']);

    if (count($fullname) == 3) {
        $firstName = strtoupper($fullname[1]);
        $lastName = strtolower($fullname[0][0]);
        $middleName = strtolower($fullname[2][0]);

        $data['status'] = true;
        $data['result'] = $firstName . ' ' . $lastName . '. ' . $middleName;
    } else {
        $data['result'] = 'Данные введены не правильно!';
    }
} else {
    $data['result'] = 'Введите данные!';
}

echo json_encode($data);