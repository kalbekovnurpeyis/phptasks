<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>PHP tasks</title>
</head>
<body>

<div class="container">
    <div class="text-center">
        <h1>PHP tasks</h1>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="accordion" id="tasks">

                <div class="card">
                    <div class="card-header" id="task1">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask1" aria-expanded="false" aria-controls="collapseTask1">
                            Тask #1
                        </button>
                    </div>
                    <div id="collapseTask1" class="collapse" aria-labelledby="task1" data-parent="#tasks">
                        <h5 class="card-header">Вам нужно разработать программу, которая считала бы сумму цифр числа введенного пользователем. Например: есть число 123, то программа должна вычислить сумму цифр 1, 2, 3, т. е. 6. (По желанию можете сделать проверку на корректность введения данных пользователем.)</h5>
                        <div class="card-body">
                            <form action="digitsum.php" method="post" id="task1Form">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="number" name="number" placeholder="Введите число">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="task2">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask2" aria-expanded="false" aria-controls="collapseTask2">
                            Тask #2
                        </button>
                    </div>
                    <div id="collapseTask2" class="collapse" aria-labelledby="task2" data-parent="#tasks">
                        <h5 class="card-header">Создать два инпута, для ввода двух чисел. Вам нужно разработать программу, которая считала бы количество вхождений какой-нибуть выбранной вами цифры в выбранном вами числе. Например: цифра 4 в числе 12345678912344 встречается 3 раза</h5>
                        <div class="card-body">
                            <form action="digitsearch.php" method="post" id="task2Form">
                                <div class="row">
                                    <div class="col-5">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="digit" name="digit" placeholder="Введите цифру">
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="number" name="number" placeholder="Введите число">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="task3">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask3" aria-expanded="false" aria-controls="collapseTask3">
                            Тask #3
                        </button>
                    </div>
                    <div id="collapseTask3" class="collapse" aria-labelledby="task3" data-parent="#tasks">
                        <h5 class="card-header">Создайте 3 инпута. Первые два инпута для ввода двух чисел, которые являются числами от и до. Третий инпут для числа, которое должно делиться без остатка на числа введеные от и до. Например введено от 1 до 9 делить на 3. Числа 3, 6, 9 деляться, результат 1, 2, 3 сумма равна 6. Функция fmod для определения "делится число" или "не делится".</h5>
                        <div class="card-body">
                            <form action="fmod.php" method="post" id="task3Form">
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="minNum" name="minNum" placeholder="Начальное число">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="maxNum" name="maxNum" placeholder="Конечное число">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="number" name="number" placeholder="Число которое надо делить">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="task4">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask4" aria-expanded="false" aria-controls="collapseTask4">
                            Тask #4
                        </button>
                    </div>
                    <div id="collapseTask4" class="collapse" aria-labelledby="task4" data-parent="#tasks">
                        <h5 class="card-header">Создайте инпут, для ввода числа. Программа должна создать массив длинной указаной в инпут, наполнить его случайными значениями (можно использовать функцию rand), найти максимальное и минимальное значение массива и поменять их местами. Вывести все на страницу</h5>
                        <div class="card-body">
                            <form action="randarray.php" method="post" id="task4Form">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="number" name="number" placeholder="Введите число">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="task5">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask5" aria-expanded="false" aria-controls="collapseTask5">
                            Тask #5
                        </button>
                    </div>
                    <div id="collapseTask5" class="collapse" aria-labelledby="task5" data-parent="#tasks">
                        <h5 class="card-header">Создать инпут для ввода ФИО. Вывести Сокращенные ФИО. Например, вводим: Василий Петрович Пупкин, вывод: ПУПКИН в. п. В верхнем регистре имя, в нижнем инициалы</h5>
                        <div class="card-body">
                            <form action="fullnamecut.php" method="post" id="task5Form">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Ф.И.О.">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="task6">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask6" aria-expanded="false" aria-controls="collapseTask6">
                            Тask #6
                        </button>
                    </div>
                    <div id="collapseTask6" class="collapse" aria-labelledby="task6" data-parent="#tasks">
                        <h5 class="card-header">Разработайте программу, которая определяла количество прошедших часов по введенным пользователем градусах. Введенное число может быть от 0 до 360.</h5>
                        <div class="card-body">
                            <form action="degreesclock.php" method="post" id="task6Form">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="degrees" name="degrees" placeholder="Введите число в градусах">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="task7">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask7" aria-expanded="false" aria-controls="collapseTask7">
                            Тask #7
                        </button>
                    </div>
                    <div id="collapseTask7" class="collapse" aria-labelledby="task7" data-parent="#tasks">
                        <h5 class="card-header">Работа светофора запрограммирована таким образом: с начала каждого часа, в течении трех минут горит зеленый сигнал, следующие две минуты горит красный, дальше в течении трех минут - зеленый и т. д. Вам нужно разработать программу, которая по введенному числу определяла какого цвета сейчас горит сигнал.</h5>
                        <div class="card-body">
                            <form action="trafficlight.php" method="post" id="task7Form">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="number" name="number" placeholder="Введите число">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="task8">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask8" aria-expanded="false" aria-controls="collapseTask8">
                            Тask #8
                        </button>
                    </div>
                    <div id="collapseTask8" class="collapse" aria-labelledby="task8" data-parent="#tasks">
                        <h5 class="card-header">Вам нужно разработать программу, которая проверяла бы введенное пользователем число (год). Число может быть в диапазоне от 1 до 9999. В результате должно быть показано високосный ли год это</h5>
                        <div class="card-body">
                            <form action="yearcheck.php" method="post" id="task8Form">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="number" name="number" placeholder="Введите год">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="task9">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask9" aria-expanded="false" aria-controls="collapseTask9">
                            Тask #9
                        </button>
                    </div>
                    <div id="collapseTask9" class="collapse" aria-labelledby="task9" data-parent="#tasks">
                        <h5 class="card-header">Игральным картам присвоены следующие порядковые номера в зависимости от их достоинства: "валет" - 11, "дама" - 12, "король" - 13, "туз" - 14. Порядковые номера остальных карт соответствуют их названиям("семерка", "восмерка" и т. д.). Вам нужно разработать программу, которая выводила достоинство карты по заданному номеру, который будет вводит пользователь.</h5>
                        <div class="card-body">
                            <form action="cardstack.php" method="post" id="task9Form">
                                <div class="row">
                                    <div class="col-10">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="number" name="number" placeholder="Введите год">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="submit" class="btn btn-primary" style="width: 100%;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script>
function request(form) {
    let method = form.attr('method');
    let action = form.attr('action');
    let data = form.serializeArray();

    let dataArray = {};

    data.forEach((e) => {
        dataArray[e.name] = e.value;
    });

    return new Promise((resolve, reject) => {
        $.ajax({
            url: action,
            type: method,
            dataType: 'json',
            data: dataArray,
        }).done(resolve).fail(reject);
    });
}

$(async () => {
    $('form').submit(async (e) => {
        e.preventDefault();

        let form = $(e.target);

        try {
            let response = await request(form);

            $('.alert').remove();

            if(response.status) {
                form.before('<div class="alert alert-success" role="alert">' + response.result + '</div>');
            } else {
                form.before('<div class="alert alert-danger" role="alert">' + response.result + '</div>');
            }
        } catch (error) {
            console.warn('Ошибка при отправке: ', error);
        }
    });
});
</script>
</body>
</html>