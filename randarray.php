<?php

$number = $_POST['number'];

$data['status'] = false;

if (is_numeric($number)) {
    if ($number > 0) {
        $arrayBefore = [];
        for ($i = 0; $i <= $number; $i++) {
            array_push($arrayBefore, rand(1, 100));
        }

        $min = array_keys($arrayBefore, min($arrayBefore))[0];
        $max = array_keys($arrayBefore, max($arrayBefore))[0];

        $arrayAfter = $arrayBefore;
        $arrayAfter[$min] = $arrayBefore[$max];
        $arrayAfter[$max] = $arrayBefore[$min];

        $data['status'] = true;
        $data['result'] = implode(', ', $arrayBefore) . '<br>' . implode(', ', $arrayAfter);
    } else {
        $data['result'] = 'Введите число больше нуля!';
    }
} else {
    $data['result'] = 'Введите числовое значение!';
}

echo json_encode($data);