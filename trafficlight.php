<?php

$number = $_POST['number'];

$data['status'] = false;

if (is_numeric($number)) {
    if ($number > 0) {
        $cycle = intval($number / 5);
        $interval = $number - $cycle * 5;

        $data['status'] = true;

        if ($interval >= 0 && $interval < 3) {
            $data['result'] = 'Зеленый';
        }

        if ($interval >= 3 && $interval < 5) {
            $data['result'] = 'Красный';
        }
    } else {
        $data['result'] = 'Введите число больше нуля!';
    }
} else {
    $data['result'] = 'Введите число!';
}

echo json_encode($data);