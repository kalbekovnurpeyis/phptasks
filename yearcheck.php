<?php

$year = $_POST['number'];

$data['status'] = false;

if (is_numeric($year)) {
    if ($year >= 1 && $year <= 9999) {
        $data['status'] = true;
        if (date("L", mktime(0, 0, 0, 7, 7, $year)) == 1) {
            $data['result'] = 'Год высокостный';
        } else {
            $data['result'] = 'Год невысокостный';
        }
    } else {
        $data['result'] = 'Введите число между 1 и 9999!';
    }
} else {
    $data['result'] = 'Введите число!';
}

echo json_encode($data);